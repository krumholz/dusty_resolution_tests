CFLAGS		= -O3 -MP
LDFLAGS		= -O3

# These tests work for mac or linux
OS              := $(shell uname)
ifeq ($(OS), Darwin)
	CLIBFLAGS	= 
	DYNLIBFLAG	= -dynamiclib
	LIBEXT		= dylib
else ifeq ($(OS), Linux)
        CLIBFLAGS	= -fPIC
        DYNLIBFLAG	= -shared
        LIBEXT		= so
endif


SOURCES         = shell_sim.c
OBJECTS         = $(SOURCES:%.c=%.o)

lib:	$(OBJECTS)
	$(CC) $(LDFLAGS) $(DYNLIBFLAG) -o shell_sim.$(LIBEXT) $^

