# Resolution tests for simulations of radiation-inhibited dusty accretion flows #

This repository contains the code used for "Resolution Requirements and Resolution Problems in Simulations of Radiative Feedback in Dusty Gas", Krumholz (2018).

### Contents ###

This repository contains the following:

* ``Makefile`` -- a make file for the c code
* ``dusty_resolution_tests.ipynb`` -- a jupyter / python notebook that carries out all the tests and produces all the figures in the paper
* ``infall_sol.py`` -- a python module that implements the analytic infall solution derived in the paper
* ``shell_sim.c`` -- c source code for the simulations
* ``shell_sim.py`` -- python wrapper for shell_sim.c

### Using the code ###

Use is very straightforward. First compile the c source code by typing ``make``. 
his will produce a dynamcally-linked library that can be called from python. Then load the
``dusty_resolution_tests.ipynb`` notebook. This carries out all the simulations in the
paper, and produces all the plots.

### License ###

This repository is distributed under the terms of the GPL v3. A copy of the license is included in the repository.

### Contact information ###

Please contact Mark Krumholz, mark.krumholz@anu.edu.au, with any questions.