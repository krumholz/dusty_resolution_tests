"""
This module implements the analytic inflow solution derived in
Krumholz (2018).
"""

# List of all routines provided
__all__ = [ 'u_inf', 'rho_inf', 'm_inf', 'infall_init',
            'fpcrit_inf' ]

# Import libraries
import numpy as np
from scipy.integrate import ode, quad
from scipy.optimize import brentq

# Velocity
def u_inf(x, fE, fp, kT, xT):
    """
    Returns the velocity of the analytic infall solution
    
    Parameters
        x : float or array
            position
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
            
    Returns
        u : float or array
            infall velocity
    """
    u1p = -np.sqrt(2.0 * (1.0 - fE/(1.0+2.0*kT)*(1.0+2.0*kT-2.0*kT/xT)))
    u1m = u1p + 1.0/fp
    u2 = (x >= xT) * \
         (2.0/x) * (1.0 - fE/(1.0+2.0*kT)*(x/xT)**(-2.0*kT)) + \
         np.logical_and(x>=1, x<xT) * \
         (2.0/x) * (1.0 - fE/(1.0+2.0*kT)*(1.0+2.0*kT-2.0*kT*x/xT)) + \
         (x < 1) * \
         (u1m**2 + 2.0*(1.0/x-1))
    return -np.sqrt(u2)

# Density
def rho_inf(x, fE, fp, ftau, eta, kT, xT):
    """
    Returns the density of the analytic infall solution
    
    Parameters
        x : float or array
            position
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        ftau : float
            optical depth parameter
        eta : float
            ratio of UV to IR opacity
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
            
    Returns
        rho : float or array
            density
    """
    return -eta*fE*fp/(ftau * x**2*u_inf(x, fE, fp, kT, xT))

# Helper function called by m_infall; not intended to be used directly
def m_inf_integrand(x, fE, fp, kT, xT):
    return -1.0/u_inf(x, fE, fp, kT, xT)

def m_inf(x, fE, fp, ftau, eta, kT, xT, x0=0.0):
    """
    Returns the total mass interior to radius x in the
    analytic infall solution
    
    Parameters
        x : float or array
            position
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        ftau : float
            optical depth parameter
        eta : float
            ratio of UV to IR opacity
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
        x0 : float
            mass returned is the mass between radius
            x0 and radius x
            
    Returns
        m : float or array
            mass
    """
    return (4*np.pi*fE*fp*eta/ftau) * \
        quad(m_inf_integrand, x0, x, args=(fE,fp,kT,xT))[0]

# Auxiliary function used by infall_init; not inteded to be called directly
def infall_cell_mass_resid(x, x0, fE, fp, ftau, eta, kT, xT, dm):
    return m_inf(x, fE, fp, ftau, eta, kT, xT, x0=x0) - dm

# Infall solution initializer; this initializes a discretized version
# of the analytic infall solution. This discretization has the following
# properties, where dm = mass of a cell, rho_inf = exact density from
# infall solution, u_inf = exact velocity from infall solution, and the
# infall solution is valid in the limit eta -> infinity (i.e., UV opacity
# is infinite compared to IR opacity)
# (1) \int_{x_i^{x_{i+1}} 4 pi r^2 rho_inf dr = dm
# (2) (1/dm) \int_{x_i^{x_{i+1}} 4 pi r^2 rho_inf u_inf dr = u
def infall_init(dm, npt, x0, fE, fp, ftau, eta, kT, xT):
    """
    This routine initializes a mesh of points where each cell contains
    equal mass and the density is uniform.
    
    Parameters
        dm : float
            mass per sample point
        npt : float
            number of points
        x0 : float
            position of first point
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        ftau : float
            optical depth parameter
        eta : float
            ratio of UV to IR opacity
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
            
    Returns
        xu : array
            array of cell center positions (first half of array) and
            velocities (second half of array)
    """
    
    # Set up grid of cell edge positions, and get position of inner edge
    x = np.zeros(npt)
    x[0] = x0
    
    # Get positions of remaining points
    for i in range(1,npt):
        if x[i-1] == 0.0:
            xlim = 100.0
        else:
            rho0 = rho_inf(x[i-1], fE, fp, ftau, eta, kT, xT)
            xlim = 2.0 * x[i-1] * \
                   (1.0 + 3.0*dm/(8.0*np.pi*rho0*x[i-1]**3))**(2./3.)
        x[i] = brentq(infall_cell_mass_resid, x[i-1], xlim,
                      args=(x[i-1], fE, fp, ftau, eta, kT, xT, dm))
        
    # Get velocities
    u = np.zeros(npt)
    for i in range(npt):
        u[i] = u_inf(x[i], fE, fp, kT, xT)
    
    # Return
    xu = np.concatenate((x,u))
    return xu


# Critical fp value
def fpcrit_inf(fE, kT, xT):
    """
    Returns the critical fp value for infall

    Parameters
        fE : float or array
            Eddington ratio parameter
        kT : float or array
            temperature slope parameter
        xT : float or array
            temperature normalization parameter

    Returns:
        fpcrit : float or array
            critical fp
    """
    return 1.0 / np.sqrt(2.0*(1.0 - fE/(1.0-2.0*kT/(xT*(1.0+2*kT)))))


# k_IR function as defined in the paper; not intended to be called
# directly, just provided for internal use
def kIR(x,kT,xT):
    if x < 1:
        return 0.0
    elif x < xT:
        return 1.0
    else:
        return (x/xT)**(-2.0*kT)

# Internal utility functions; not intended to be used directly, just
# used as inputs to quad for numerical integration
def dp_grav_inf_integrand(x, fE, fp, kT, xT):
    return 1.0/(x**2 * u_inf(x, fE, fp, kT, xT))

def dp_IR_inf_integrand(x, fE, fp, kT, xT):
    return -kIR(x, kT, xT) /(x**2 * u_inf(x, fE, fp, kT, xT))

# Integration functions; called the versions without the extension
# _scalar, not these directly
def dp_grav_inf_scalar(x, fE, fp, kT, xT):
    if x > 1:
        dp = quad(dp_grav_inf_integrand, x, np.inf, args=(fE,fp,kT,xT))[0]
    else:
        dp = quad(dp_grav_inf_integrand, 1, np.inf, args=(fE,fp,kT,xT))[0]
        dp += quad(dp_grav_inf_integrand, x, 1, args=(fE,fp,kT,xT))[0]
    return dp

def dp_IR_inf_scalar(x, fE, fp, kT, xT):
    dp = fE*quad(dp_IR_inf_integrand, max(x,1), np.inf, args=(fE,fp,kT,xT))[0]
    return dp

# Functions to calculate momentum deposition
def dp_grav_inf(x, fE, fp, kT, xT):
    """
    Calculate the total change in momentum per unit mass delivered to
    a fluid element as it falls from infinity to radius x as a result
    of gravity

    Parameters
        x : float or array
            dimensionless radius
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter

    Returns
        dp_grav : float or array
            the total change in momentum per unit mass experienced by
            a fluid element as it travels from infinity to x, as a
            result of gravity
    """
    if hasattr(x, '__iter__'):
        return np.array(
            [ dp_grav_inf_scalar(x_, fE, fp, kT, xT) for x_ in x ])
    else:
        return dp_grav_inf_scalar(x, fE, fp, kT, xT)
    

def dp_IR_inf(x, fE, fp, kT, xT):
    """
    Calculate the total change in momentum per unit mass delivered to
    a fluid element as it falls from infinity to radius x as a result
    of IR radiation

    Parameters
        x : float or array
            dimensionless radius
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter

    Returns
        dp_IR : float or array
            the total change in momentum per unit mass experienced by
            a fluid element as it travels from infinity to x, as a
            result of IR radiation
    """
    if hasattr(x, '__iter__'):
        return np.array(
            [ dp_IR_inf_scalar(x_, fE, fp, kT, xT) for x_ in x ])
    else:
        return dp_IR_inf_scalar(x, fE, fp, kT, xT)

    
def dp_UV_inf(x, fp):
    """
    Calculate the total change in momentum per unit mass delivered to
    a fluid element as it falls from infinity to radius x as a result
    of direct stellar radiation

    Parameters
        x : float or array
            dimensionless radius
        fE : float
            Eddington ratio parameter
        fp : float
            momentum ratio parameter
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter

    Returns
        dp_UV : float or array
            the total change in momentum per unit mass experienced by
            a fluid element as it travels from infinity to x, as a
            result of direct stellar radiation
    """
    if hasattr(x, '__iter__'):
        dp = np.zeros(np.asarray(x).shape)
        dp[np.asarray(x) < 1.0] = 1.0/fp
    else:
        if x < 1.0:
            dp = 1.0/fp
        else:
            dp = 0.0
    return dp
