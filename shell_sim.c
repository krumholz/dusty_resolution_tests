/* This code implements the simple 1D integration method described in
   Krumholz (2018). The code is intended to be called from python,
   which handles the initialization and IO. Important note: this code
   was written to be clear as a demonstration, NOT for
   efficiency. MANY things here could be done more efficiently and
   with a smaller memory footprint, but were written this way to
   maximize readability and clarity. */

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/***********************************************************************/
/* Structure to hold problem parameters                                */
/***********************************************************************/

struct integrator_params {
  int nx;
  double dm, fE, ftau, eta, kT, xT, nu, cs2;
  bool use_subgrid;
};

/***********************************************************************/
/* Force computation routines                                          */
/***********************************************************************/

/* Gravity */
void get_f_grav(int nx, double *x, double dm, int nsink,
		double *f_grav) {
  for (int i=0; i<nx; i++)
    f_grav[i] = -(1.0 + (i+nsink)*dm) / (x[i]*x[i]);
}

/* Pressure */
void get_f_pres(int nx, double *x, double dm, double cs2,
		double *f_pres) {

  double rho_l, rho_r;
  rho_l = 3.0*dm /
    (4.0 * M_PI * (x[1]*x[1]*x[1] - x[0]*x[0]*x[0]));
  for (int i=1; i<nx-1; i++) {
    rho_r = 3.0*dm /
      (4.0 * M_PI * (x[i+1]*x[i+1]*x[i+1] - x[i]*x[i]*x[i]));
    f_pres[i] = -4.0*M_PI*x[i]*x[i] * cs2 * (rho_r - rho_l) / dm;
    rho_l = rho_r;
  }
}

/* UV force */
void get_f_UV(double *x, double *u, struct integrator_params *par,
	      double *f_UV) {
  
  /* If using the subgrid model, estimate the mass flux onto the
     accretion zone */
  if (par->use_subgrid) {
    /* Compute mass accretion rate and fp */
    
    double rho0h = 3.0*par->dm /
      (4.0*M_PI * (x[1]*x[1]*x[1] - x[0]*x[0]*x[0]));
    double x0h = pow(0.5*(x[0]*x[0]*x[0] + x[1]*x[1]*x[1]), 1./3.);
    double u0h = 0.5*(u[0] + u[1]);
    double mdot = -4.0*M_PI * x0h*x0h * u0h * rho0h;
    /*
    double rho0 = 3.0*par->dm/(8.0*M_PI*x[0]*x[0]*x[0] *
			       (1.0 - pow(x[1]/x[0], -1.5)));
    double mdot = 4.0*M_PI*x[0]*x[0]*u[0]*rho0;
    */
    double fp = mdot * par->ftau / (4.0*M_PI*par->fE * par->eta);
    /* Compare mass flux to critical value */
    double fpcrit = 1.0 /
      sqrt(2.0*(1.0 - par->fE/
		(1.0 - 2.0*par->kT/(par->xT*(1.0+2*par->kT)))));
    /* If mass accretion rate is too high, radiation force is zero */
    if (fp > fpcrit) {
      for (int i=0; i<par->nx-1; i++) f_UV[i] = 0.0;
      return;
    }
  }
  double col_i = 0.0;
  double etau_i = 1.0;
  for (int i=0; i<par->nx-1; i++) {
    double rho = 3.0*par->dm /
      (4.0*M_PI * (x[i+1]*x[i+1]*x[i+1] - x[i]*x[i]*x[i]));
    double col_ip1 = col_i +
      (fmax(x[i+1],1.0)-fmax(x[i],1.0)) * rho;
    double etau_ip1 = exp(-par->ftau*col_ip1);
    f_UV[i] = 4.0*M_PI * par->eta * par->fE / (par->ftau*par->dm) *
      (etau_i - etau_ip1);
    col_i = col_ip1;
    etau_i = etau_ip1;
  }
}

/* IR force */
void get_f_IR(int nx, double *x, double dm, double fE, double ftau,
	      double kT, double xT, double *f_IR) {
  double col = 0.0;
  for (int i=0; i<nx-1; i++) {
    if (x[i] >= 1.0) {
      f_IR[i] = fE * (1.0-exp(-ftau*col)) *
	pow(fmax(x[i]/xT, 1.0), -2.0*kT) / (x[i]*x[i]);
    }
    col += (fmax(x[i+1],1.0)-fmax(x[i],1.0)) * 3.0*dm /
      (4.0*M_PI * (x[i+1]*x[i+1]*x[i+1] - x[i]*x[i]*x[i]));
  }
  if (x[nx-1] >= 1.0) {
    f_IR[nx-1] = fE * (1.0-exp(-ftau*col)) *
      pow(fmax(x[nx-1]/xT, 1.0), -2.0*kT) / (x[nx-1]*x[nx-1]);
  }
}

/* Viscosity force */
void get_f_visc(int nx, double *x, double *u, double dm, double nu,
		double *f_visc) {

  /* Initialize viscous flux in first zone */
  double du = u[1]-u[0];
  double x_l = 0.5*(x[1]+x[0]);
  double q_l = 0.0;
  if (du < 0) {
    double rho = 3.0*dm /
      (4.0 * M_PI * (x[1]*x[1]*x[1] - x[0]*x[0]*x[0]));
    q_l = nu * rho * du*du;
  }

  /* Loop over cells */
  for (int i=1; i<nx-1; i++) {

    /* Get viscous flux on right of interface */
    double q_r = 0.0;
    du = u[i+1] - u[i];
    if (du < 0) {
      double rho = 3.0*dm /
	(4.0 * M_PI * (x[i+1]*x[i+1]*x[i+1] - x[i]*x[i]*x[i]));
      q_r = nu * rho * du*du;
    }

    /* Get viscous force */
    double x_r = 0.5*(x[i+1]+x[i]);
    f_visc[i] = -4.0*M_PI * (x_r*x_r*q_r - x_l*x_l*q_l) / dm;

    /* Rotate right to left */
    x_l = x_r;
    q_l = q_r;
  }  
}


/***********************************************************************/
/* Integration routines                                                */
/***********************************************************************/

/* Sink routine -- this cuts zones that have fallen into the sink
   region out of the data array */
int apply_sink(int nx, double *xu, double xsink) {

  /* Figure out how many zones have fallen into sink region */
  int nsink = 0;
  while (xu[nsink] < xsink && nsink < nx) nsink++;

  /* Move up the stuff that remains in the xu array, so it occupies
     the first 2*(nx - nsink) slots */
  if (nsink > 0) {
    for (int i=nsink; i<nx; i++) xu[i-nsink] = xu[i];
    for (int i=nsink; i<nx; i++) xu[i-nsink+nx-nsink] = xu[i+nx];
  }

  /* Return number of zones removed */
  return nsink;
}

/* This saves the current state of the calculation, properly
   accounting for sink zones that have been removed */
void save_state(int nx, int nx_init, double xsink,
		double *xu, double *xu_save) {
  int nx_sink = nx_init - nx;
  for (int i=0; i<nx_sink; i++) {
    xu_save[i] = xsink;
    xu_save[i+nx_init] = 0.0;
  }
  for (int i=nx_sink; i<nx_init; i++) {
    xu_save[i] = xu[i-nx_sink];
    xu_save[i+nx_init] = xu[i-nx_sink+nx];
  }
}


/* This routine advances the calculation a single time step, using a
   second-order accurate method. It computes the time step on the fly,
   using the calculated derivatives. */
#define NDENSESTATE 7
double timestep(double *xu, double cfl, int nsink, double dt_min,
		double dt_max, struct integrator_params *par,
		double t, int nx_init, double xsink, int ndense,
		int *dense_pts, double *dense_hist) {

  /* Allocate workspace */
  double *xu_save = calloc(2*par->nx, sizeof(double));
  double *xu_h = calloc(2*par->nx, sizeof(double));
  double *xu_proj = calloc(2*par->nx, sizeof(double));
  double *f_grav = calloc(par->nx, sizeof(double));
  double *f_grav_h = calloc(par->nx, sizeof(double));
  double *f_grav_proj = calloc(par->nx, sizeof(double));
  double *f_UV = calloc(par->nx, sizeof(double));
  double *f_UV_h = calloc(par->nx, sizeof(double));
  double *f_UV_proj = calloc(par->nx, sizeof(double));
  double *f_IR = calloc(par->nx, sizeof(double));
  double *f_IR_h = calloc(par->nx, sizeof(double));
  double *f_IR_proj = calloc(par->nx, sizeof(double));
  double *f_visc = calloc(par->nx, sizeof(double));
  double *f_visc_h = calloc(par->nx, sizeof(double));
  double *f_visc_proj = calloc(par->nx, sizeof(double));
  double *f_pres = calloc(par->nx, sizeof(double));
  double *f_pres_h = calloc(par->nx, sizeof(double));
  double *f_pres_proj = calloc(par->nx, sizeof(double));
  double *udot = calloc(par->nx, sizeof(double));
  double *udot_h = calloc(par->nx, sizeof(double));
  double *udot_proj = calloc(par->nx, sizeof(double));
  
  /* Convenience pointers */
  double *x = xu;
  double *u = xu + par->nx;
  double *x_h = xu_h;
  double *u_h = xu_h + par->nx;
  double *x_proj = xu_proj;
  double *u_proj = xu_proj + par->nx;
  
  /* Save the current state, in case we need to back up and try again */
  for (int i=0; i<2*par->nx; i++) xu_save[i] = xu[i];

  /* Calculate forces at start of time step */
  get_f_grav(par->nx, x, par->dm, nsink, f_grav);
  get_f_UV(x, u, par, f_UV);
  get_f_IR(par->nx, x, par->dm, par->fE, par->ftau, par->kT, par->xT,
	   f_IR);
  get_f_visc(par->nx, x, u, par->dm, par->nu, f_visc);
  get_f_pres(par->nx, x, par->dm, par->cs2, f_pres);
  for (int i=0; i<par->nx; i++)
    udot[i] = f_grav[i] + f_UV[i] + f_IR[i] + f_visc[i] + f_pres[i];

  /* Calculate time step constraint */
  double dt = DBL_MAX;
  for (int i=0; i<par->nx-1; i++) {
    double dx = x[i+1] - x[i];
    double du = fabs(u[i+1] - u[i]) + sqrt(par->cs2);
    double da = fabs(udot[i+1] - udot[i]);
    double dt_cell = cfl * (-du + sqrt(du*du + 2.0*da*dx)) / da;
    dt = fmin(dt, dt_cell);
  }
  bool max_step = false;
  if (dt_max > 0.0) {
    if (dt >= dt_max) {
      dt = dt_max;
      max_step = true;
    }
  }

  /* Make sure time step succeeds, or fails gracefully */
  while (dt >= dt_min) {

    /* Advance to half step */
    for (int i=0; i<par->nx; i++) {
      x_h[i] = x[i] + 0.5 * dt * u[i];
      u_h[i] = u[i] + 0.5 * dt * udot[i];
    }

    /* Check that shells have not crossed in the projected state,
       because this makes it impossible to compute the forces
       sensibly; if a shell crossing is detected, go back and try
       again with a smaller time step */
    bool shell_cross = false;
    for (int i=0; i<par->nx-1; i++) {
      if (x_h[i+1] <= x_h[i]) {
	shell_cross = true;
	break;
      }
    }
    if (shell_cross) {
      dt /= 2.0;
      max_step = false;
      continue;
    }

    /* Recalculate forces at half step */
    get_f_grav(par->nx, x_h, par->dm, nsink, f_grav_h);
    get_f_UV(x_h, u_h, par, f_UV_h);
    get_f_IR(par->nx, x_h, par->dm, par->fE, par->ftau, par->kT, par->xT,
	     f_IR_h);
    get_f_visc(par->nx, x_h, u_h, par->dm, par->nu, f_visc_h);
    get_f_pres(par->nx, x_h, par->dm, par->cs2, f_pres_h);
    for (int i=0; i<par->nx; i++)
      udot_h[i] = f_grav_h[i] + f_UV_h[i] + f_IR_h[i] + f_visc_h[i]
	+ f_pres_h[i];
  
    /* Advance to the projected full step */
    for (int i=0; i<par->nx; i++) {
      x_proj[i] = x[i] + dt * u_h[i];
      u_proj[i] = u[i] + dt * udot_h[i];
    }
  
    /* Check that shells have not crossed, and reduce time step if
       they have */
    for (int i=0; i<par->nx-1; i++) {
      if (x_proj[i+1] <= x_proj[i]) {
	shell_cross = true;
	break;
      }
    }
    if (shell_cross) {
      dt /= 2.0;
      max_step = false;
      continue;
    }
    
    /* Calculate the accelerations at the projected full step */
    get_f_grav(par->nx, x_proj, par->dm, nsink, f_grav_proj);
    get_f_UV(x_proj, u_proj, par, f_UV_proj);
    get_f_IR(par->nx, x_proj, par->dm, par->fE, par->ftau, par->kT, par->xT,
	     f_IR_proj);
    get_f_visc(par->nx, x_proj, u_proj, par->dm, par->nu, f_visc_proj);
    get_f_pres(par->nx, x_proj, par->dm, par->cs2, f_pres_proj);
    for (int i=0; i<par->nx; i++)
      udot_proj[i] = f_grav_proj[i] + f_UV_proj[i] + f_IR_proj[i]
	+ f_visc_proj[i] + f_pres_proj[i];

    /* Do the corrector step */
    for (int i=0; i<par->nx; i++) {
      x[i] = x_proj[i] + 0.5*dt*(u[i] - 2.0*u_h[i] + u_proj[i]);
      u[i] = u_proj[i] + 0.5*dt*(udot[i] - 2.0*udot_h[i] + udot_proj[i]);
    }
    
    /* Check that shells have not crossed */
    for (int i=0; i<par->nx-1; i++) {
      if (x[i+1] <= x[i]) {
	shell_cross = true;
	break;
      }
    }

    /* If no shell crossing, we're done */
    if (!shell_cross) break;

    /* If we're here shell crossing has occurred; restore the
       original state and try again with half the time step */
    for (int i=0; i<2*par->nx; i++) xu[i] = xu_save[i];
    dt /= 2.0;
    max_step = false;
  }

  /* If storing dense output, do so now */
  if (ndense > 0) {
    int nx_sink = nx_init - par->nx;
    dense_hist[0] = t+dt;
    for (int i=0; i<ndense; i++) {
      int off = i*NDENSESTATE;
      if (dense_pts[i] < nx_sink) {
	dense_hist[off+1] = xsink;
	dense_hist[off+2] = 0.0;
	dense_hist[off+3] = 0.0;
	dense_hist[off+4] = 0.0;
	dense_hist[off+5] = 0.0;
	dense_hist[off+6] = 0.0;
	dense_hist[off+7] = 0.0;
      } else {
	int j = dense_pts[i]-nx_sink;
	dense_hist[off+1] = x[dense_pts[i]-nx_sink];
	dense_hist[off+2] = u[dense_pts[i]-nx_sink];
	dense_hist[off+3] = 0.5*dt*(f_grav[j] + f_grav_proj[j]);
	dense_hist[off+4] = 0.5*dt*(f_UV[j] + f_UV_proj[j]);
	dense_hist[off+5] = 0.5*dt*(f_IR[j] + f_IR_proj[j]);
	dense_hist[off+6] = 0.5*dt*(f_visc[j] + f_visc_proj[j]);
	dense_hist[off+7] = 0.5*dt*(f_pres[j] + f_pres_proj[j]);
      }
    }
  }

  /* Free memory */
  free(xu_save);
  free(xu_h);
  free(xu_proj);
  free(udot);
  free(udot_h);
  free(udot_proj);
  free(f_grav);
  free(f_grav_h);
  free(f_grav_proj);
  free(f_UV);
  free(f_UV_h);
  free(f_UV_proj);
  free(f_IR);
  free(f_IR_h);
  free(f_IR_proj);
  free(f_visc);
  free(f_visc_h);
  free(f_visc_proj);
  free(f_pres);
  free(f_pres_h);
  free(f_pres_proj);

  /* Return one of the following:
     -- for a successful step of dt < dt_max: return dt
     -- for a successful step of dt = dt_max: return DBL_MAX
     -- for a failed step (dt < dt_min): return 0
   */
  if (max_step) return DBL_MAX;
  else if (dt < dt_min) return 0.0;
  else return dt;
}


/* This is the main simulation driver routine */
double run_sim(double *xu, double tf, double dt_min,
	       int nstepmax, double cfl, double xsink,
	       int nhist, int ndense, int *dense_pts,
	       bool verbose, struct integrator_params par,
	       double *xu_hist, double *dense_hist) {

  /* Save initial state to output history */
  save_state(par.nx, par.nx, xsink, xu, xu_hist);

  /* If doing dense saves, initialize the dense save array; note that
     dense saves are not compatible with nstepmax = 0 */
  if (ndense > 0) {
    if (nstepmax == 0) {
      printf("Halting simulation: ndense > 0 requires nstepmax > 0\n");
      return 0.0;
    }
    /* Store initial state, with all dp values set to zero */
    dense_hist[0] = 0.0;
    for (int i=0; i<ndense; i++) {
      dense_hist[NDENSESTATE*i+1] = xu[dense_pts[i]];
      dense_hist[NDENSESTATE*i+2] = xu[dense_pts[i]+par.nx];
      for (int j=3; j<NDENSESTATE+1; j++)
	dense_hist[NDENSESTATE*i+j] = 0.0;
    }
    /* Mark all other steps as not set yet */
    for (int i=1; i<nstepmax; i++) {
      int off = (NDENSESTATE*ndense+1)*i;
      dense_hist[off] = -1.0;
    }
  }

  /* Remove zones in sink region */
  int nx_init = par.nx;
  par.nx -= apply_sink(par.nx, xu, xsink);

  /* Run the simulation */
  int nsink = 0, histctr = 1, stepctr=0;
  double t = 0.0;
  while (t < tf) {

    /* Take a step */
    double dt = timestep(xu, cfl, nx_init - par.nx, dt_min,
			 histctr * tf / nhist - t,
			 &par, t, nx_init, xsink,
			 ndense, dense_pts,
			 dense_hist+(stepctr+1)*(ndense*NDENSESTATE+1));
  
    /* Check for failed advance; on failure, save the final state and
       then return */
    if (dt == 0.0) {
      save_state(par.nx, nx_init, xsink, xu,
		 xu_hist + 2*nx_init*histctr);
      printf("Calculation stopped at t = %f by dt < dt_min\n",
	     t);
      return t;
    }

    /* Remove zones that have fallen into sink region */
    int nsink = apply_sink(par.nx, xu, xsink);
    par.nx -= nsink;

    /* If all zones have been accreted, save the final state and then
       return */
    if (par.nx == 0) {
      save_state(par.nx, nx_init, xsink, xu,
		 xu_hist + 2*nx_init*histctr);
      printf("Calculation stopped at t = %f by accretion of all zones\n",
	     t);
      return t;
    }

    /* If the most recent time step brought us to an output time, save
       output */
    if (dt == DBL_MAX)
      save_state(par.nx, nx_init, xsink, xu,
		 xu_hist + 2*nx_init*histctr);

    /* Advance time */
    if (dt == DBL_MAX) {
      t = histctr * tf / nhist;
      histctr++;
    } else {
      t += dt;
    }

    /* Check for too many steps */
    stepctr++;
    if (stepctr > nstepmax && nstepmax > 0) {
      save_state(par.nx, nx_init, xsink, xu,
		 xu_hist + 2*nx_init*histctr);
      printf("Calculation stopped at t = %f by nstep > nstep_max\n",
	     t);
      return t;
    }

    /* Print status if verbose */
    if (verbose) {
      if (stepctr % 100 == 0) {
	printf("Advanced to t = %f in %d steps; %d zones accreted\n",
	       t, stepctr, nx_init - par.nx);
      }
    }
  }

  /* Print status if verbose */
  if (verbose) {
    printf("Calculation completed in %d steps; %d zones accreted\n",
	   stepctr, nx_init - par.nx);
  }

  /* Return */
  return t;
}
