"""
This module defines python wrappers to the shell_sim c routines
"""

# List of routines provided
__all__ = [
    # Loading routine
    'loadlib',
    # Force computations
    'f_grav', 'f_UV', 'f_IR', 'f_visc', 'f_pres'
    # Integration machinery
    'timestep', 'run_sim'
    ]

# Imports
import numpy as np
import numpy.ctypeslib as npct
from ctypes import c_double, c_int, c_bool, pointer, POINTER, Structure
import os

# Utility type definition
array_1d_double = npct.ndpointer(dtype=np.double, ndim=1,
                                 flags="CONTIGUOUS")
array_1d_int = npct.ndpointer(dtype=c_int, ndim=1,
                              flags="CONTIGUOUS")

# Define a structure used in the c code
class integrator_params(Structure):
    _fields_ = [ ("nx", c_int),
                 ("dm", c_double),
                 ("fE", c_double),
                 ("ftau", c_double),
                 ("eta", c_double),
                 ("kT", c_double),
                 ("xT", c_double),
                 ("nu", c_double),
                 ("cs2", c_double),
                 ("use_subgrid", c_bool) ]

# Persistent value to hold pointer to c library
__libptr = None

#########################################################################
# Routine to load the library and define an interface to it             #
#########################################################################

def loadlib(path=None):
    """
    Function to load the library

    Parameters
       path : string
          path to library; default is current directory

    Returns
       nothing
    """

    # Point to global libptr
    global __libptr

    # Do nothing if already loaded
    if __libptr is not None:
        return

    # Set path if not specified
    if path is None:
        path = os.path.dirname(__file__)

    # Load library
    __libptr = npct.load_library("shell_sim",
                                 os.path.realpath(path))

    # Define interface to functions
    __libptr.get_f_grav.restype = None
    __libptr.get_f_grav.argtypes = [ c_int, array_1d_double,
                                     c_double, c_int, array_1d_double ]
    __libptr.get_f_UV.restype = None
    __libptr.get_f_UV.argtypes = [ c_int, array_1d_double, c_double,
                                   c_double, c_double, c_double,
                                   array_1d_double ]
    __libptr.get_f_IR.restype = None
    __libptr.get_f_IR.argtypes = [ c_int, array_1d_double, c_double,
                                   c_double, c_double, c_double,
                                   c_double, array_1d_double ]
    __libptr.get_f_visc.restype = None
    __libptr.get_f_visc.argtypes = [ c_int, array_1d_double,
                                     array_1d_double, c_double,
                                     c_double, array_1d_double ]
    __libptr.get_f_pres.restype = None
    __libptr.get_f_pres.argtypes = [ c_int, array_1d_double,
                                     c_double, c_double,
                                     array_1d_double ]
    __libptr.timestep.restype = c_double
    __libptr.timestep.argtypes  = [ array_1d_double, c_double,
                                    c_double, c_double,
                                    POINTER(integrator_params),
                                    c_double, c_int, c_double,
                                    c_int, array_1d_int,
                                    array_1d_double ]
    __libptr.run_sim.restype = c_double
    __libptr.run_sim.argtypes = [ array_1d_double,
                                  c_double, c_double, c_int,
                                  c_double, c_double, c_int,
                                  c_int, array_1d_int,
                                  c_bool, integrator_params,
                                  array_1d_double, array_1d_double ]
    
#########################################################################
# Python wrappers to c routines                                         #
#########################################################################

# Force computation routines
def f_grav(x, dm, xsink=0.5):
    """
    Compute gravitational force on a cell
    
    Parameters
        x : array
            cell center positions
        dm : float
            cell mass
        xsink : float
            radius of sink region; forces will be zero for cells
            inside sink region
        
    Returns
        f_g : array
            gravitational force per unit mass
    """
    loadlib()
    idx = x > xsink
    nsink = len(x) - np.sum(idx)
    ngood = len(x) - nsink
    f_grav = np.zeros(ngood)
    __libptr.get_f_grav(ngood, x[nsink:], dm, nsink, f_grav)
    f_grav = np.concatenate((np.zeros(nsink), f_grav))
    return f_grav

def f_pres(x, dm, cs2, xsink=0.5):
    """
    Compute gravitational force on a cell
    
    Parameters
        x : array
            cell center positions
        dm : float
            cell mass
        cs2 : float
            square of sound speed
        xsink : float
            radius of sink region; forces will be zero for cells
            inside sink region
        
    Returns
        f_p : array
            pressure force per unit mass
    """
    loadlib()
    idx = x > xsink
    nsink = len(x) - np.sum(idx)
    ngood = len(x) - nsink
    f_p = np.zeros(ngood)
    __libptr.get_f_pres(ngood, x[nsink:], dm, cs2, f_p)
    f_p = np.concatenate((np.zeros(nsink), f_p))
    return f_p

def f_UV(x, dm, fE, ftau, eta, xsink=0.5):
    """
    Compute the UV radiative force on a cell
    
    Parameters
        x : array
            cell center positions
        dm : float
            cell mass
        fE : float
            Eddington ratio parameter
        ftau : float
            optical depth parameter
        eta : float
            ratio of UV to IR opacity
        xsink : float
            radius of sink region; forces will be zero for cells
            inside sink region
            
    Returns
        f_UV : array
            UV radiation force per unit mass
    """
    loadlib()
    idx = x > xsink
    nsink = len(x) - np.sum(idx)
    ngood = len(x) - nsink
    f_UV = np.zeros(ngood)
    __libptr.get_f_UV(ngood, x[nsink:], dm, fE, ftau, eta, f_UV)
    f_UV = np.concatenate((np.zeros(nsink), f_UV))
    return f_UV

def f_IR(x, dm, fE, ftau, kT, xT, xsink=0.5):
    """
    Compute the IR radiative force on a cell
    
    Parameters
        x : array
            cell center positions
        dm : float
            cell mass
        fE : float
            Eddington ratio parameter
        ftau : float
            optical depth parameter            
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
        xsink : float
            radius of sink region; forces will be zero for cells
            inside sink region
            
    Returns
        f_IR : array
            IR radiation force per unit mass
    """
    loadlib()
    idx = x > xsink
    nsink = len(x) - np.sum(idx)
    ngood = len(x) - nsink
    f_IR = np.zeros(ngood)
    __libptr.get_f_IR(ngood, x[nsink:], dm, fE, ftau, kT, xT, f_IR)
    f_IR = np.concatenate((np.zeros(nsink), f_IR))
    return f_IR

def f_visc(x, u, dm, nu, xsink=0.5):
    """
    Function to compute artificial viscosity force
    
    Parameters
        x : array
            positions of cell centers
        u : array
            velocities of cell centers
        dm : float
            cell mass
        nu : float
            magnitude of artificial viscosity
        xsink : float
            radius of sink region; forces will be zero for cells
            inside sink region
            
    Returns :
        f_visc : array
            artifical viscosity forces per unit mass
    """
    loadlib()
    idx = x > xsink
    nsink = len(x) - np.sum(idx)
    ngood = len(x) - nsink
    f_visc = np.zeros(ngood)
    __libptr.get_f_visc(ngood, x[nsink:], u[nsink:], dm, nu, f_visc)
    f_visc = np.concatenate((np.zeros(nsink), f_visc))
    return f_visc


# Integrator routines
def timestep(xu, dm, fE, ftau, eta, kT, xT, nu=2.0, cs2=1.0e-4,
             cfl=0.5, dt_min=1.0e-8, dt_max=0.0):
    """
    Routine to advance the simulation one time step

    Parameters
        xu : array, shape (N)
            initial positions and velocities
        dm : float
            cell mass
        fE : float
            Eddington ratio parameter
        ftau : float
            opacity parameter       
        eta : float
            ratio of UV to IR opacity
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
        nstep : int
            number of output steps
        nu : float
            magnitude of artificial viscosity
        cs2 : float
            squared sound speed
        cfl : float
            CFL number
        dt_min : float
            minimum time step to allow
        dt_max : float
            maximum time step to take; a value of 0 means no maximum
            is applied

    Returns
        dt : float
            either the time step that was taken, DBL_MAX (if the time
            step wound up being equal to dt_max), or 0.0 (if the time
            step failed, because the CFL condition implied dt <
            dt_min)
    """
    loadlib()
    t = 0.0
    par = integrator_params(xu.size//2, dm, fE, ftau, eta, kT,
                            xT, nu, cs2)
    dt = __libptr.timestep(xu, cfl, dt_min, dt_max, pointer(par))
    return dt


def run_sim(xu, t, dm, fE, ftau, eta, kT, xT, nu=2.0,
            cs2=1.0e-4, xsink=0.5, cfl=0.5, dt_min=1.0-8,
            nstepmax=100000, nhist=100, densepts=None,
            use_subgrid=False, verbose=False):
    """
    Driver routine to run simulations
    
    Parameters
        xu : array, shape (N)
            initial positions and velocities
        t : float
            time for which to run simulation
        dm : float
            cell mass
        fE : float
            Eddington ratio parameter
        ftau : float
            opacity parameter       
        eta : float
            ratio of UV to IR opacity
        kT : float
            temperature slope parameter
        xT : float
            temperature normalization parameter
        nu : float
            magnitude of artificial viscosity
        cs2 : float
            squared sound speed
        xsink : float
            inner sink radius; shells that get to this radius
            are no longer followed
        cfl : float
            CFL number
        nhist : int
            number of output times past the initial time
        dt_min : float
            minimum time step to allow
        nstepmax : int
            maximum number of time steps to take
        densepts : listlike
            list of point numbers to be saved densely, where all
            information is returned at every time step
        use_subgrid : bool
            if True, the subgrid model of Krumholz (2018) is used to
            determine whether to turn on UV radiation force
        verbose : bool
            if True, print status messages
            
    Returns
        xu_hist : array, shape (M, N)
            history of xu values at output times
        tf : float
            final time reached in the integration; will be equal to
            the input t if integration was successful, but may be
            smaller if integration terminated early
        dense_data : dict (only included if ndense > 0)
            a dict containing the dense data output; the dict contains
            the keys 't', 'x', 'u', 'du_grav', 'du_UV', 'du_IR',
            du_pres', and 'du_visc', which contain the time, positions,
            velocities, and velocity changes during that step due to
            gravity, UV radiation, IR radiation, pressure, and
            viscosity for each dense history point; 't' contains the
            output times, and is a 1D array of (P) elements; all the
            other keys is an array of shape (Q, P), where there are Q
            dense history points

    Notes
        Integration runs until (1) the time reaches t, (2) the
        integration time step dt becomes smaller than dt_min, or (3)
        the number of integration time steps exceeds nstepmax. The
        termination condition determines the output times in the
        xu_hist array. If the integration runs to completion
        (condition 1), there are M = nhist+1 outputs, uniformly spaced
        in time from 0 - t. If it terminates early (conditions 2 or
        3), the first M - 1 slices in xu_hist are the same as if the
        calculation ran to completion, and xu_hist[M-1,:] contains the
        state of the calculation at the final time step, corresponding
        to time tf.
    """
    loadlib()

    # Prepare parameters
    par = integrator_params(xu.size//2, dm, fE, ftau, eta, kT,
                            xT, nu, cs2, use_subgrid)

    # Prepare holders for output history
    xu_hist = np.zeros(xu.size * (nhist+1))
    if densepts is None:
        ndense = 0
        densepts_arr = np.zeros(0, dtype=c_int)
        dense_hist = np.zeros(0)
    else:
        ndense = len(densepts)
        densepts_arr = np.array(densepts, dtype=c_int)
        dense_hist = np.zeros(nstepmax*(ndense*7+1))
        
    # Run simulation
    tf = __libptr.run_sim(xu, t, dt_min, nstepmax, cfl, xsink,
                          nhist, ndense, densepts_arr, verbose,
                          par, xu_hist, dense_hist)

    # Clean up history output
    xu_hist = xu_hist.reshape((nhist+1, xu.size))
    if t != tf:
        # Cut off empty outputs if run terminated early
        nhist_done = int(np.ceil(tf/(t/nhist)))
        xu_hist = xu_hist[:nhist_done+1,:]

    # Organize dense point output
    if ndense > 0:
        ndat = 1+7*ndense
        t_dense = dense_hist[::ndat]
        ntime = np.argmax(t_dense < 0.0)
        t_dense = t_dense[:ntime]
        data_pattern = np.add.outer(np.arange(ntime)*ndat,
                                    np.arange(ndense)*7).flatten()
        dense_data = {
            't' : t_dense,
            'x' : np.transpose(
                dense_hist[data_pattern+1].reshape((ntime,ndense))),
            'u' : np.transpose(
                dense_hist[data_pattern+2].reshape((ntime,ndense))),
            'du_grav' : np.transpose(
                dense_hist[data_pattern+3].reshape((ntime,ndense))),
            'du_UV' : np.transpose(
                dense_hist[data_pattern+4].reshape((ntime,ndense))),
            'du_IR' : np.transpose(
                dense_hist[data_pattern+5].reshape((ntime,ndense))),
            'du_visc' : np.transpose(
                dense_hist[data_pattern+6].reshape((ntime,ndense))),
            'du_pres' : np.transpose(
                dense_hist[data_pattern+7].reshape((ntime,ndense)))
        }
        
    # Return
    if ndense == 0:
        return xu_hist, tf
    else:
        return xu_hist, tf, dense_data
